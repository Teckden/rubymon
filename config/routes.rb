Rails.application.routes.draw do

  namespace :api do
    resources :monsters
    resources :monster_teams
  end

  resources :monster_teams
  resources :monsters
  devise_for :users, controller: { omniauth_callbacks: 'users/omniauth_callbacks'  }
  root 'pages#index'
end
