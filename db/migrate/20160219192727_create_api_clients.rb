class CreateApiClients < ActiveRecord::Migration
  def change
    create_table :api_clients do |t|
      t.string :title
      t.string :token

      t.timestamps null: false
    end
  end
end
