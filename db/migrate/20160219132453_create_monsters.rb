class CreateMonsters < ActiveRecord::Migration
  def change
    create_table :monsters do |t|
      t.string :name
      t.string :type
      t.string :power
      t.string :weakness
      t.references :user
      t.timestamps null: false
    end

    add_index :monsters, :user_id
  end
end
