require 'rails_helper'

module Api
  describe MonsterTeamsController, type: :request do
    before(:all) { @token = FactoryGirl.create(:api_client).token }

    let(:parsed_response) { JSON.parse(response.body) }

    describe 'GET /api/monster_teams' do
      let(:user) { FactoryGirl.create(:user) }
      let!(:monster_team) { FactoryGirl.create(:monster_team, user: user) }

      before { get "/api/monster_teams?email=#{user.email}&token=#{@token}" }

      it 'returns monster teams list for the user' do
        monster_team_attrs = MonsterTeamSerializer.new(monster_team)
                                          .serializable_hash.with_indifferent_access

        expect(parsed_response['monster_teams'].count).to eq(1)
        expect(parsed_response['monster_teams'][0]).to eq(monster_team_attrs)
      end

      it { expect(response.status).to eq(200) }
    end

    describe 'GET /api/monster_teams/:id' do
      let!(:monster_team) { FactoryGirl.create(:monster_team) }

      before { get "/api/monster_teams/#{monster_team.id}?token=#{@token}" }

      it { expect(response.status).to eq(200) }

      it 'returns monster' do
        monster_team_attrs = MonsterTeamSerializer.new(monster_team)
                            .serializable_hash.with_indifferent_access
        expect(parsed_response['monster_team']).to eq(monster_team_attrs)
      end
    end

    describe 'POST /api/monster_teams' do
      let(:user) { FactoryGirl.create(:user) }

      context 'with valid attrs' do
        let(:valid_attrs) { FactoryGirl.attributes_for(:monster_team, user_id: user.id) }

        before do
          3.times { FactoryGirl.create(:monster, user: user) }
          valid_attrs.merge!(monster_ids: Monster.pluck(:id))

          post "/api/monster_teams?token=#{@token}", monster_team: valid_attrs
        end

        it { expect(response.status).to eq(200) }

        it 'creates monster team' do
          expect(MonsterTeam.count).to eq(1)
          expect(MonsterTeam.last.name).to eq(valid_attrs[:name])
        end

        it 'returns monster team' do
          monster_team_attrs = MonsterTeamSerializer.new(MonsterTeam.last)
                              .serializable_hash.with_indifferent_access
          expect(parsed_response['monster_team']).to eq(monster_team_attrs)
        end
      end

      context 'with invalid attrs' do
        let(:invalid_attrs) { FactoryGirl.attributes_for(:monster_team, power: nil) }

        before { post "/api/monster_teams?token=#{@token}", monster_team: invalid_attrs }

        it { expect(response.status).to eq(422) }

        it 'returns errors' do
          expect(parsed_response['monster_team']['errors'])
              .to eq('base' => ['Monster team should consists of 3 monsters'])
        end
      end
    end

    describe 'PUT /api/monster_teams/:id' do
      let(:monster_team) { FactoryGirl.create(:monster_team) }

      context 'with valid attrs' do
        let(:valid_attrs) { Hash(name: 'New name') }

        before { put "/api/monster_teams/#{monster_team.id}?token=#{@token}", monster_team: valid_attrs }

        it { expect(response.status).to eq(200) }

        it 'updates monster team' do
          expect(monster_team.reload.name).to eq('New name')
        end

        it 'returns monster with updated attrs' do
          monster_team_attrs = MonsterTeamSerializer.new(monster_team.reload)
                              .serializable_hash.with_indifferent_access

          expect(parsed_response['monster_team']).to eq(monster_team_attrs)
        end
      end

      context 'with invalid attrs' do
        let(:invalid_attrs) { Hash(name: nil) }

        before { put "/api/monster_teams/#{monster_team.id}?token=#{@token}", monster_team: invalid_attrs }

        it { expect(response.status).to eq(422) }

        it 'should not update monster team' do
          expect(monster_team.reload.name).to_not eq(nil)
        end

        it 'returns errors' do
          expect(parsed_response['monster_team']['errors']).to eq('name' => ["can't be blank"])
        end
      end
    end

    describe 'DELETE /api/monster_teams/:id' do
      let(:monster_team) { FactoryGirl.create(:monster_team) }

      before { delete "/api/monster_teams/#{monster_team.id}?token=#{@token}" }

      it { expect(response.status).to eq(200) }

      it 'deletes monster team' do
        expect(MonsterTeam.count).to eq(0)
      end
    end
  end
end

