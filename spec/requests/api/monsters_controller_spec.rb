require 'rails_helper'

module Api
  describe MonstersController, type: :request do
    before(:all) { @token = FactoryGirl.create(:api_client).token }

    let(:parsed_response) { JSON.parse(response.body) }

    describe 'GET /api/monsters' do
      let(:user) { FactoryGirl.create(:user) }
      let!(:monster) { FactoryGirl.create(:monster, user: user) }

      before { get "/api/monsters?email=#{user.email}&token=#{@token}" }

      it 'returns monsters list for the user' do
        monster_attrs = MonsterSerializer.new(monster)
                            .attributes.with_indifferent_access

        expect(parsed_response['monsters'].count).to eq(1)
        expect(parsed_response['monsters'][0]).to eq(monster_attrs)
      end

      it { expect(response.status).to eq(200) }
    end

    describe 'GET /api/monsters/:id' do
      let!(:monster) { FactoryGirl.create(:monster) }

      before { get "/api/monsters/#{monster.id}?token=#{@token}" }

      it { expect(response.status).to eq(200) }

      it 'returns monster' do
        monster_attrs = MonsterSerializer.new(monster)
                            .attributes.with_indifferent_access
        expect(parsed_response['monster']).to eq(monster_attrs)
      end
    end

    describe 'POST /api/monsters' do
      let(:user) { FactoryGirl.create(:user) }

      context 'with valid attrs' do
        let(:valid_attrs) { FactoryGirl.attributes_for(:monster, user_id: user.id) }

        before { post "/api/monsters?token=#{@token}", monster: valid_attrs }

        it { expect(response.status).to eq(200) }

        it 'creates monster' do
          expect(Monster.count).to eq(1)
          expect(Monster.last.name).to eq(valid_attrs[:name])
        end

        it 'returns monster' do
          monster_attrs = MonsterSerializer.new(Monster.last)
                              .attributes.with_indifferent_access
          expect(parsed_response['monster']).to eq(monster_attrs)
        end
      end

      context 'with invalid attrs' do
        let(:invalid_attrs) { FactoryGirl.attributes_for(:monster, power: nil) }

        before { post "/api/monsters?token=#{@token}", monster: invalid_attrs }

        it { expect(response.status).to eq(422) }

        it 'returns errors' do
          expect(parsed_response['monster']['errors']).to eq('power' => ["can't be blank"])
        end
      end
    end

    describe 'PUT /api/monsters/:id' do
      let(:monster) { FactoryGirl.create(:monster) }

      context 'with valid attrs' do
        let(:valid_attrs) { Hash(name: 'New name') }

        before { put "/api/monsters/#{monster.id}?token=#{@token}", monster: valid_attrs }

        it { expect(response.status).to eq(200) }

        it 'updates monster' do
          expect(monster.reload.name).to eq('New name')
        end

        it 'returns monster with updated attrs' do
          monster_attrs = MonsterSerializer.new(monster.reload)
                              .attributes.with_indifferent_access

          expect(parsed_response['monster']).to eq(monster_attrs)
        end
      end

      context 'with invalid attrs' do
        let(:invalid_attrs) { Hash(power: nil) }

        before { put "/api/monsters/#{monster.id}?token=#{@token}", monster: invalid_attrs }

        it { expect(response.status).to eq(422) }

        it 'should not update monster' do
          expect(monster.reload.power).to_not eq(nil)
        end

        it 'returns errors' do
          expect(parsed_response['monster']['errors']).to eq('power' => ["can't be blank"])
        end
      end
    end

    describe 'DELETE /api/monsters/:id' do
      let(:monster) { FactoryGirl.create(:monster) }

      before { delete "/api/monsters/#{monster.id}?token=#{@token}" }

      it { expect(response.status).to eq(200) }

      it 'deletes monster' do
        expect(Monster.count).to eq(0)
      end
    end
  end
end

