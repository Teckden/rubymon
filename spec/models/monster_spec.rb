require 'rails_helper'

RSpec.describe Monster, type: :model do

  it 'has a valid factory' do
    expect(FactoryGirl.build(:monster)).to be_valid
  end

  it { is_expected.to validate_inclusion_of(:type).in_array(%w(fire wind earth electric water)) }
  it { is_expected.not_to allow_value('unknown type').for(:type) }

  it 'should not create monster if user has already had 20 monsters' do
    user = FactoryGirl.create(:user)
    20.times { FactoryGirl.create(:monster, user: user) }

    expect(FactoryGirl.build(:monster, user: user)).to_not be_valid
  end

  it 'should set weakness' do
    monster = FactoryGirl.build(:monster, type: 'wind')
    expect(monster.weakness).to eq(nil)

    monster.save
    expect(monster.reload.weakness).to eq('fire')
  end
end
