require 'rails_helper'

RSpec.describe ApiClient, type: :model do
  it 'has a valid factory' do
    expect(FactoryGirl.build(:api_client)).to be_valid
  end

  it 'sets token on create' do
    client = FactoryGirl.build(:api_client)
    expect { client.save }.to change { client.token }
  end
end
