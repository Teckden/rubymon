require 'rails_helper'

RSpec.describe MonsterTeam, type: :model do
  it 'has a valid factory' do
    monster_team = FactoryGirl.build(:monster_team)
    expect(monster_team).to be_valid
    expect(monster_team.monsters.size).to eq(3)
  end

  it 'should not be valid if there is not enough monsters' do
    monster_team = FactoryGirl.build(:monster_team)
    monster_team.monsters = []
    expect(monster_team.monsters.size).to eq(0)
    expect(monster_team).to_not be_valid
  end

  it 'should not be valid if there is more than 3 monsters' do
    monster_team = FactoryGirl.build(:monster_team)
    monster_team.monsters << FactoryGirl.build(:monster)
    expect(monster_team.monsters.size).to eq(4)
    expect(monster_team).to_not be_valid
  end

  it 'should not be valid if user already has 3 teams' do
    user = FactoryGirl.create(:user)
    3.times { FactoryGirl.create(:monster_team, user: user) }
    expect(FactoryGirl.build(:monster_team, user_id: user.id)).to_not be_valid
  end

  describe '::for' do
    let(:user) { FactoryGirl.create(:user) }

    before do
      FactoryGirl.create(:monster_team, user: user)
      FactoryGirl.create(:monster_team, user: user)
      FactoryGirl.create(:monster_team)
    end

    it 'returns all monster teams based on user email' do
      expect(MonsterTeam.for(user.email).count).to eq(2)
    end
  end
end
