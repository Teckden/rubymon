require 'rails_helper'
require 'ostruct'

RSpec.describe User, type: :model do

  it 'has a valid user factory' do
    expect(FactoryGirl.build(:user)).to be_valid
  end

  describe '::from_facebook' do
    let(:info) { OpenStruct.new(email: 'user@example.com' ) }
    let(:auth) { OpenStruct.new(provider: 'facebook', uid: '1',
                                info: info) }

    it 'creates user from facebook params' do
      expect { described_class.from_facebook(auth) }.to change { User.count }.by(1)
    end

    it 'should not create user if user exists' do
      FactoryGirl.create(:user, provider: auth.provider, uid: auth.uid)
      expect { described_class.from_facebook(auth) }.to_not change { User.count }
    end

    it 'should return user' do
      expect(described_class.from_facebook(auth)).to eq(User.first)
    end
  end
end
