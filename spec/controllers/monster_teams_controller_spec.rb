require 'rails_helper'

RSpec.describe MonsterTeamsController, type: :controller do
  include Devise::TestHelpers

  before { sign_in FactoryGirl.create(:user) }

  describe "GET #index" do
    it "assigns all monster_teams as @monster_teams" do
      monster_team = FactoryGirl.create(:monster_team, user: User.first)
      get :index
      expect(assigns(:monster_teams)).to eq([monster_team])
    end
  end

  describe "GET #show" do
    it "assigns the requested monster_team as @monster_team" do
      monster_team = FactoryGirl.create(:monster_team, user: User.first)
      get :show, { id: monster_team.to_param }
      expect(assigns(:monster_team)).to eq(monster_team)
    end
  end

  describe "GET #new" do
    it "assigns a new monster_team as @monster_team" do
      get :new
      expect(assigns(:monster_team)).to be_a_new(MonsterTeam)
      expect(assigns[:monster_team].user).to eq(User.first)
    end
  end

  describe "GET #edit" do
    it "assigns the requested monster_team as @monster_team" do
      monster_team = FactoryGirl.create(:monster_team, user: User.first)
      get :edit, { id: monster_team.to_param }
      expect(assigns(:monster_team)).to eq(monster_team)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      let(:valid_attributes) { FactoryGirl.attributes_for(:monster_team) }

      before do
        3.times { FactoryGirl.create(:monster, user: User.first) }
        valid_attributes.merge!(monster_ids: Monster.pluck(:id))
      end

      it "creates a new MonsterTeam" do
        expect {
          post :create, { monster_team: valid_attributes }
        }.to change(MonsterTeam, :count).by(1)
        expect(MonsterTeam.last.user).to eq(User.first)
      end

      it "assigns a newly created monster_team as @monster_team" do
        post :create, { monster_team: valid_attributes }
        expect(assigns(:monster_team)).to be_a(MonsterTeam)
        expect(assigns(:monster_team)).to be_persisted
      end

      it "redirects to the created monster_team" do
        post :create, { monster_team: valid_attributes }
        expect(response).to redirect_to(MonsterTeam.last)
      end
    end

    context "with invalid params" do
      let(:invalid_attributes) { FactoryGirl.attributes_for(:monster_team, name: nil) }

      it "assigns a newly created but unsaved monster_team as @monster_team" do
        post :create, { monster_team: invalid_attributes }
        expect(assigns(:monster_team)).to be_a_new(MonsterTeam)
      end

      it "re-renders the 'new' template" do
        post :create, { monster_team: invalid_attributes }
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) { Hash(name: 'new name') }

      it "updates the requested monster_team" do
        monster_team = FactoryGirl.create(:monster_team, user: User.first)
        put :update, { id: monster_team.to_param, monster_team: new_attributes }
        expect(monster_team.reload.name).to eq('new name')
      end

      it "assigns the requested monster_team as @monster_team" do
        monster_team = FactoryGirl.create(:monster_team, user: User.first)
        put :update, { id: monster_team.to_param, monster_team: new_attributes }
        expect(assigns(:monster_team)).to eq(monster_team)
      end

      it "redirects to the monster_team" do
        monster_team = FactoryGirl.create(:monster_team, user: User.first)
        put :update, { id: monster_team.to_param, monster_team: new_attributes }
        expect(response).to redirect_to(monster_team)
      end
    end

    context "with invalid params" do
      let(:invalid_attributes) { Hash(name: nil) }

      it "assigns the monster_team as @monster_team" do
        monster_team = FactoryGirl.create(:monster_team, user: User.first)
        put :update, { id: monster_team.to_param, monster_team: invalid_attributes }
        expect(assigns(:monster_team)).to eq(monster_team)
      end

      it "re-renders the 'edit' template" do
        monster_team = FactoryGirl.create(:monster_team, user: User.first)
        put :update, { id: monster_team.to_param, monster_team: invalid_attributes }
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested monster_team" do
      monster_team = FactoryGirl.create(:monster_team, user: User.first)
      expect {
        delete :destroy, { id: monster_team.to_param }
      }.to change(MonsterTeam, :count).by(-1)
    end

    it "redirects to the monster_teams list" do
      monster_team = FactoryGirl.create(:monster_team, user: User.first)
      delete :destroy, { id: monster_team.to_param }
      expect(response).to redirect_to(monster_teams_url)
    end
  end
end
