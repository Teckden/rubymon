require 'rails_helper'

RSpec.describe MonstersController, type: :controller do
  include Devise::TestHelpers

  before { sign_in FactoryGirl.create(:user) }

  describe "GET #index" do
    it "assigns all monsters as @monsters" do
      monster = FactoryGirl.create(:monster, user: User.first)
      get :index
      expect(assigns(:monsters)).to eq([monster])
    end
  end

  describe "GET #show" do
    it "assigns the requested monster as @monster" do
      monster = FactoryGirl.create(:monster, user: User.first)
      get :show, { id: monster.to_param }
      expect(assigns(:monster)).to eq(monster)
    end
  end

  describe "GET #new" do
    it "assigns a new monster as @monster" do
      get :new
      expect(assigns(:monster)).to be_a_new(Monster)
      expect(assigns[:monster].user_id).to eq(User.first.id)
    end
  end

  describe "GET #edit" do
    it "assigns the requested monster as @monster" do
      monster = FactoryGirl.create(:monster, user: User.first)
      get :edit, { id: monster.to_param }
      expect(assigns(:monster)).to eq(monster)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      let(:valid_attributes) { FactoryGirl.attributes_for(:monster) }

      it "creates a new Monster" do
        expect {
          post :create, { monster: valid_attributes }
        }.to change(Monster, :count).by(1)
        expect(Monster.last.user).to eq(User.first)
      end

      it "assigns a newly created monster as @monster" do
        post :create, { monster: valid_attributes }
        expect(assigns(:monster)).to be_a(Monster)
        expect(assigns(:monster)).to be_persisted
      end

      it "redirects to the created monster" do
        post :create, { monster: valid_attributes }
        expect(response).to redirect_to(Monster.last)
      end
    end

    context "with invalid params" do
      let(:invalid_attributes) { FactoryGirl.attributes_for(:monster, name: nil) }

      it "assigns a newly created but unsaved monster as @monster" do
        post :create, { monster: invalid_attributes }
        expect(assigns(:monster)).to be_a_new(Monster)
      end

      it "re-renders the 'new' template" do
        post :create, { monster: invalid_attributes }
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) { Hash(name: 'New name') }

      it "updates the requested monster" do
        monster = FactoryGirl.create(:monster, user: User.first)
        put :update, { id: monster.to_param, monster: new_attributes }
        expect(monster.reload.name).to eq('New name')
      end

      it "assigns the requested monster as @monster" do
        monster = FactoryGirl.create(:monster, user: User.first)
        put :update, { id: monster.to_param, monster: new_attributes }
        expect(assigns(:monster)).to eq(monster)
      end

      it "redirects to the monster" do
        monster = FactoryGirl.create(:monster, user: User.first)
        put :update, { id: monster.to_param, monster: new_attributes }
        expect(response).to redirect_to(monster)
      end
    end

    context "with invalid params" do
      let(:invalid_attributes) { Hash(name: nil) }

      it "assigns the monster as @monster" do
        monster = FactoryGirl.create(:monster, user: User.first)
        put :update, { id: monster.to_param, monster: invalid_attributes }
        expect(assigns(:monster)).to eq(monster)
      end

      it "re-renders the 'edit' template" do
        monster = FactoryGirl.create(:monster, user: User.first)
        put :update, { id: monster.to_param, monster: invalid_attributes }
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested monster" do
      monster = FactoryGirl.create(:monster, user: User.first)
      expect {
        delete :destroy, { id: monster.to_param }
      }.to change(Monster, :count).by(-1)
    end

    it "redirects to the monsters list" do
      monster = FactoryGirl.create(:monster, user: User.first)
      delete :destroy, { id: monster.to_param }
      expect(response).to redirect_to(monsters_url)
    end
  end
end
