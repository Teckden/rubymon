FactoryGirl.define do
  factory :monster_team do
    name "My team"
    after(:build) do |monster_team|
      monster_team.monsters << build_list(:monster, 3, user_id: monster_team.user_id)
    end
  end
end
