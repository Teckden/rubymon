FactoryGirl.define do
  factory :monster do
    name "Monster"
    type "fire"
    power "1"
  end
end
