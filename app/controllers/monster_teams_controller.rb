class MonsterTeamsController < ApplicationController
  before_action :monster_team, only: [:show, :edit, :update, :destroy]

  def index
    @monster_teams = current_user.monster_teams
  end

  def show
  end

  def new
    @monster_team = current_user.monster_teams.new
  end

  def edit
  end

  def create
    @monster_team = current_user.monster_teams.new(monster_team_params)

    if @monster_team.save
      redirect_to @monster_team, notice: 'Monster team was successfully created.'
    else
      render :new
    end
  end

  def update
    if @monster_team.update(monster_team_params)
      redirect_to @monster_team, notice: 'Monster team was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @monster_team.destroy
    redirect_to monster_teams_url, notice: 'Monster team was successfully destroyed.'
  end

  private
    def monster_team
      @monster_team = current_user.monster_teams.find(params[:id])
    end

    def monster_team_params
      params.require(:monster_team).permit(:name, monster_ids: [])
    end
end
