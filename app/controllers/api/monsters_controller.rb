module Api
  class MonstersController < ApiController
    def index
      monsters = Monster.joins(:user).where(users: { email: params[:email] })
      render json: monsters, each_serializer: MonsterSerializer
    end

    def show
      monster = Monster.find(params[:id])
      render json: monster, serializer: MonsterSerializer
    end

    def create
      monster = Monster.new(monster_params)

      if monster.save
        render json: monster, serializer: MonsterSerializer
      else
        render json: { monster: { errors: monster.errors } },
               status: 422
      end
    end

    def update
      monster = Monster.find(params[:id])

      if monster.update(monster_params)
        render json: monster, serializer: MonsterSerializer
      else
        render json: { monster: { errors: monster.errors } },
               status: 422
      end
    end

    def destroy
      monster = Monster.find(params[:id])
      monster.destroy
      render nothing: true
    end

    private

    def monster_params
      params.require(:monster).permit(:name, :power, :type, :user_id)
    end
  end
end
