module Api
  class MonsterTeamsController < ApiController
    def index
      render json: MonsterTeam.for(params[:email]),
             each_serializer: MonsterTeamSerializer
    end

    def show
      monster_team = MonsterTeam.includes(:monsters).find(params[:id])
      render json: monster_team, serializer: MonsterTeamSerializer
    end

    def create
      monster_team = MonsterTeam.new(monster_team_params)

      if monster_team.save
        render json: monster_team, serializer: MonsterTeamSerializer
      else
        render json: { monster_team: { errors: monster_team.errors } },
               status: 422
      end
    end

    def update
      monster_team = MonsterTeam.find(params[:id])

      if monster_team.update(monster_team_params)
        render json: monster_team, serializer: MonsterTeamSerializer
      else
        render json: { monster_team: { errors: monster_team.errors } },
               status: 422
      end
    end

    def destroy
      monster_team = MonsterTeam.find(params[:id])
      monster_team.destroy
      render nothing: true
    end

    private

    def monster_team_params
      params.require(:monster_team).permit(:name, :user_id, monster_ids: [])
    end
  end
end
