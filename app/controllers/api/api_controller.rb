class Api::ApiController < ApplicationController
  skip_before_action :authenticate_user!
  before_action :authenticate_request
  respond_to :json

  private

  def authenticate_request
    unless ApiClient.where(token: params[:token]).present?
      render json: { error: 'Unauthorized' }
    end
  end
end
