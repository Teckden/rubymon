require 'securerandom'

class ApiClient < ActiveRecord::Base
  validates :title, presence: true

  before_create :set_token

  private

  def set_token
    self.token = SecureRandom.uuid
  end
end
