class Monster < ActiveRecord::Base
  self.inheritance_column = nil

  TYPES = %w(fire water earth electric wind)

  belongs_to :user
  belongs_to :monster_team

  validates :name, :type, :power, presence: true
  validates :type, inclusion: { in: TYPES }
  validate do |monster|
    if monster.user && monster.user.monsters.count >= 20
      errors.add(:base, 'User can have maximum 20 monsters')
    end
  end

  before_save :set_weakness

  private

  def set_weakness
    current_type_index = TYPES.index(self.type)
    if self.type == TYPES.last
      self.weakness = TYPES.first
    else
      self.weakness = TYPES[current_type_index + 1]
    end
  end
end
