class MonsterTeam < ActiveRecord::Base
  belongs_to :user
  has_many :monsters

  validates :name, presence: true
  validate do |monster_team|
    if monster_team.monsters && monster_team.monsters.size != 3
      errors.add(:base, 'Monster team should consists of 3 monsters')
    end
  end

  validate do |monster_team|
    if monster_team.user && monster_team.user.monster_teams.size >= 3
      errors.add(:base, 'User is not allowed to have more than three monster teams')
    end
  end

  scope :for, -> (email) { joins(:user).where(users: { email: email }).includes(:monsters) }
end
