class MonsterSerializer < ActiveModel::Serializer
  attributes :id, :name, :type, :power, :weakness
end
