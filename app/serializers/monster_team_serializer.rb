class MonsterTeamSerializer < ActiveModel::Serializer
  attributes :id, :name

  has_many :monsters, each_serializer: MonsterSerializer
end
