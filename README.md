# Rubymon

Rubymon is a mobile app that allows players to catch, collect monsters, and eventually
battle them. The game is played on a mobile app but we want to store user and monster
information in the cloud. We need you to create a RESTful backend API to store and
organize the monsters.

### Installation Requirements
- node
- npm
- bower
- ruby 2.2 and higher

### To Get Started
```sh
$ bundle install
```
```sh
$ rake bower:install
```
> *This project uses bower to manage its assets. View bower-rails docs for more information
